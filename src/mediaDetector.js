$(function() {


	function MediaDetector() {

		this.init();

	}


	MediaDetector.prototype = {

		mediaParam : {

			iphone   : 'screen and (max-width: 600px), screen and (min-device-width: 320px) and (max-device-width: 480px), screen and (device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1), screen and (device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)',
			ipad     : 'screen and (max-width: 960px), screen and (max-device-width: 1024px) and (min-device-width: 768px), screen and (-webkit-min-device-pixel-ratio: 2) and (max-device-width: 1024px) and (min-device-width: 768px), screen and (-webkit-min-device-pixel-ratio: 1) and (max-device-width: 1024px) and (min-device-width: 768px), screen and (max-device-width: 1024px) and (min-device-width: 768px) and (orientation: landscape)'
			

		},


		init : function() {

			var
			_self = this;


			this
			.mediaDelta = false;

			this
			.detectMedia();

			this
			.startWatch();

		},


		startWatch : function() {

			$(window).on('resize', this.detectMedia.bind(this));

		},


		detectMedia : function() {

			var
			_self = this,
			delta = 'desktop';


			$.each(_self.mediaParam, function(key, value) {

				if ( window.matchMedia( value ).matches ) {

					delta = key;
					return false;

				}

			});


			this.changeMedia( delta );

		},


		changeMedia : function(delta) {

			if ( this.mediaDelta === delta ) return false;


			this.mediaDelta = delta;


			$(window).trigger( 'changeMedia', delta );
			

		}


	};


	window
	.mediaDetector = new MediaDetector();

});